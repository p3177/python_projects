import socket
import glitter

SERVER_IP = "54.187.16.171"
SERVER_PORT = 1336
ENTER_INFO = '100#{gli&&er}{"user_name":"WRXD18","password":"yahavHaTomtom","enable_push_notifications":true}##'
CHECKSUM_INFO = '110#{gli&&er}1776##'
SEARCH_FLAG = '300#{gli&&er}{"search_type":"SIMPLE","search_entry":"k"}##'
FEED_LOAD = '310#{gli&&er}1192##'
GLANCE_STAT = '400#{gli&&er}[1192,2482]##'
FOLLOW_REQ = '410#{gli&&er}[1192,2482]##'
FOLLOW_REQ_FOR_ME = '410#{gli&&er}[CHANGEHERE,1192]##'
ENTE_CHANGE = '550#{gli&&er}{"feed_owner_id":1192,"publisher_id":1192,"publisher_screen_name":"WHATYWHAT","publisher_avatar":"im5","background_color":"white","date":"2021-06-17T14:24:54.666Z","content":"BIG DUMMY","font_color":"black","id":-1}##'
GLIT_PUBLISH_PICTURE = '550#{gli&&er}{"feed_owner_id":1192,"publisher_id":1192,"publisher_screen_name":"WHATYWHAT","publisher_avatar":"im4","background_color":"black","date":"2021-06-17T14:24:54.666Z","content":"http://cyber.glitter.org.il/assets/images/im4.png","font_color":"black","id":-1}##'
GLIT_PUBLISH_BACKGROUND = '550#{gli&&er}{"feed_owner_id":1192,"publisher_id":1192,"publisher_screen_name":"WHATYWHAT","publisher_avatar":"im4","background_color":"black","date":"2021-06-17T14:24:54.666Z","content":"BIG DUMMY","font_color":"black","id":-1}##'
LIKE_AMOUNT_FLAG = '710#{gli&&er}{"glit_id":12853,"user_id":1192,"user_screen_name":"WHATYWHAT","id":-1}##'
GLIT_PUBLISH_TEXT = '550#{gli&&er}{"feed_owner_id":1192,"publisher_id":1192,"publisher_screen_name":"WHATYWHAT","publisher_avatar":"im4","background_color":"white","date":"2021-06-17T14:24:54.666Z","content":"mayve","font_color":"pink","id":-1}##'
PUSH_NOTI_OFF = '100#{gli&&er}{"user_name":"WRXD18","password":"yahavHaTomtom","enable_push_notifications":false}##'

choice = None
def main():
    global choice
#sanity check for while loop.
    sanity_check = 'y'
    while sanity_check == 'y':
# checks for a valid option flag before starting communication posses.
        while choice is None:
            choice = glitter.greeting()
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
            glitter.communication_pointer(sock, choice)
        choice = None
        sanity_check = input("\nWould you like to use again?(if so, press 'y'): ")


if __name__ == "__main__":
    main()



