# -*- coding: utf-8 -*-
"""
Created on Tue Apr 27 21:31:21 2021

@author: magshimim
"""
#version 1.0: constant respond with slight bug issues.
#version 2.0: fixed bug issues. No exeptions added.

LOCAL_LISTEN_PORT = 9090
LOCAL_SERVER_IP = "127.0.0.1"
COMMANDS = {
"Get Albums": "ALB",
"Get Album Songs" : "SNG",
"Get Song Length" : "LEN",
"Get Song Lyrics" : "LYR",
"Get Song Album" : "ALS",
"Search Song By Name" : "SEA",
"Search Song By Lyrics" : "SE1",
"Quit" : "QUI"
}


import socket

def main():
    print("PINK FLOYD SERVER - ACTIVE.");
    while True:
        server_msg = ""
        with socket.socket() as PF_info_server:
            PF_info_server.bind((LOCAL_SERVER_IP, LOCAL_LISTEN_PORT))
            server_msg = client_communication(PF_info_server, server_msg)
    print("PINK FLOYD SERVER - INACTIVE.");

def client_communication(PF_info_server, server_msg):
         PF_info_server.listen(1)
         client_soc, _ = PF_info_server.accept()
         print("connection was formed" )
         with client_soc:
             server_msg = "Welcome!"
             while server_msg != COMMANDS["Quit"]:
                 server_action, server_msg = client_request(PF_info_server, server_msg, client_soc)
                 client_soc.sendall(server_action.encode())
         
 
def client_request(PF_info_server, server_msg, client_soc):
    if server_msg == "Welcome!":
        client_soc.sendall(server_msg.encode())
        server_msg = "none"
    client_answer = client_soc.recv(1024).decode()
    server_action = COMMANDS[client_answer]
    return server_action, server_msg
    


if __name__ == "__main__":
    main()

