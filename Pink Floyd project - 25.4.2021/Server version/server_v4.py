# -*- coding: utf-8 -*-
"""
Created on Tue Apr 27 21:31:21 2021

@author: magshimim
"""
#version 1.0: constant respond with slight bug issues.
#version 2.0: fixed bug issues, no exeptions added.
#version 3.0(pressented version): raised exeptions, final 'basic connection' 
#version 4.0: login bonus + minor bug fixes.

#dict according to client answers (possible answers only) + port and ip.
LOCAL_LISTEN_PORT = 9090
LOCAL_SERVER_IP = "127.0.0.1"
COMMANDS = {
"Get Albums": "ALB",
"Get Album Songs" : "SNG",
"Get Song Length" : "LEN",
"Get Song Lyrics" : "LYR",
"Get Song Album" : "ALS",
"Search Song By Name" : "SEA",
"Search Song By Lyrics" : "SE1",
"Quit" : "QUI"
}
PASSWORD = "Arnold Layne"

import socket
import Data
import hashlib

def main():
    print("PINK FLOYD SERVER - ACTIVE\n");
    while True:
        server_msg = ""
#opening "PF_info_server" as an object for function usege
        with socket.socket() as PF_info_server:
            PF_info_server.bind((LOCAL_SERVER_IP, LOCAL_LISTEN_PORT))
#using client_communication() function
            server_msg = client_communication(PF_info_server, server_msg)

"""
input: PF_info_server(object), server_msg
output: server_msg
function is responsible for basic communication with client.
""" 
def client_communication(PF_info_server, server_msg):
    correct_password = False
    PF_info_server.listen(2)
    client_soc, _ = PF_info_server.accept()
    print("Connection was formed." )
    with client_soc:   
        while not correct_password:
            try:    
                correct_password = login_bonus(client_soc)
                if not correct_password:
                    client_soc.sendall("False".encode())
            except Exception: 
                print("problem with client answer - incorrect password.\n~~~~~~~~~~~~~~~~")
                return server_msg
        if correct_password:
            server_msg = "Welcome! to the:"
            while True:
                try:                 
                    server_action, server_msg = client_request(PF_info_server, server_msg, client_soc)
                except Exception:
                    return
                client_soc.sendall(server_action.encode())
         
def login_bonus(client_soc):
    client_answer = client_soc.recv(1024).decode()
    correct_password = hashlib.md5(PASSWORD.encode('utf8')).hexdigest()
    if correct_password == client_answer:
        return True
        
"""
input: PF_info_server(object), server_msg, client_soc(object)
output: server_action, server_msg
function is responsible for dealing with client request.
""" 
def client_request(PF_info_server, server_msg, client_soc):
    if server_msg == "Welcome! to the:":
        client_soc.sendall(server_msg.encode())
        server_msg = "none"
#expected error when reciving data from user.
    try:
        client_answer = client_soc.recv(1024).decode().split("#")
        action = client_answer[0]
        data = client_answer[1]
    except Exception:
        print("connection was aborted.\n~~~~~~~~~~~~~~~~")
        return
#see if answer is a function that the srever reconizes.    
    try:
        server_action = command_pointer(COMMANDS[action], data)
    except Exception:
        print("problem with client answer.\n~~~~~~~~~~~~~~~~")
        return
    return server_action, server_msg

def command_pointer(action, data):
    if action == "ALB":
        return Data.ALB()
    elif action == "SNG":
        return Data.SNG(data.lower())
    elif action == "LEN":
        return Data.LEN(data)
    elif action == "LYR":
        return Data.LYR(data)
    elif action == "ALS":
        return Data.ALS(data)
    elif action == "SEA":
        return Data.SEA(data.lower())
    elif action == "SE1":
        return Data.SE1(data.lower())
    
    


if __name__ == "__main__":
    main()

