# -*- coding: utf-8 -*-
"""
Created on Mon May  3 13:21:39 2021

@author: magshimim
"""
ALBUM_IDENTIFIER = '#'
SONG_IDENTIFIER = '*'
INFO_IDENTIFIER = "::"
FILE_PATH = r"C:/Users/magshimim/Desktop/Magshimim/web and network/Semester 2/Pink Floyd project - 25.4.2021/Pink_Floyd_DB.txt"
AMOUNT_OF_ALBUMS = 8
NOT_FOUND = -1
AMOUNT_OF_WORDS = 50

import collections
import re

"""
input: none.
output: client_answer
return all the albums of Pink Floyd to server.
""" 
def ALB():
    list_of_albums = []
    with open(FILE_PATH) as data_file:
        raw_data = data_file.readlines()
        for item in raw_data:
            if item[0] == ALBUM_IDENTIFIER:
                list_of_albums.append((item.split(INFO_IDENTIFIER)[0])[1:])
        client_answer = "LIST OF ALBUMS: " + ', '.join(map(str,list_of_albums)) + "."
        return client_answer

"""
input: data.
output: client_answer
return all the songs of the requested album.
""" 
def SNG(data):
    songs_list = "REQUESTED ALBUM DISCOGRAPHY: "
    with open(FILE_PATH) as data_file:
        album_found = False
        for lines in data_file:
            if ALBUM_IDENTIFIER in lines and data in lines.lower():
                album_found = True
                continue
            if ALBUM_IDENTIFIER in lines and album_found:
                break
            if album_found and SONG_IDENTIFIER in lines:
                album_songs = (lines[1:lines.index(INFO_IDENTIFIER)])
                songs_list = songs_list + album_songs  + ", " 
                
        if songs_list == "REQUESTED ALBUM DISCOGRAPHY: ":
            client_answer = "NO DISCOGRAPHY FOUND, PLEASE TRY ANOTHER ALBUM."
            return client_answer
        
        else:
            client_answer = songs_list[:len(songs_list) - 2] + "."
            return client_answer

"""
input: data.
output: client_answer
returns the length of the requested song.
"""    
def LEN(data):
    client_answer = ""
    with open(FILE_PATH) as data_file:
        for song in data_file:
            if SONG_IDENTIFIER in song:
                temp = song
                if data in temp:
                    chosen_song = temp
                    chosen_song = chosen_song.split(INFO_IDENTIFIER)
                    client_answer = "THE SONG, "  +  (chosen_song[0])[1:]  + " LENGTH IS: " + chosen_song[2] + "."
        if client_answer == "":
            client_answer =  "NO SUCH SONG FOUND IN DISCOGRAPHY."
        return client_answer
                    
         
"""
input: data.
output: client_answer
returns all the Lyrics of the requested song.
"""
def LYR(data):
    client_answer = "\nLYRICS:\n"
    song_found = False
    with open(FILE_PATH) as data_file:
        for lines_of_lyrics in data_file:
            if data in lines_of_lyrics:
                song_found = True
                continue
            if song_found:
                if ALBUM_IDENTIFIER in lines_of_lyrics or SONG_IDENTIFIER in lines_of_lyrics:
                    break
                client_answer = client_answer + lines_of_lyrics +"\n"
        if client_answer == "\nLYRICS:\n":
            client_answer = "NO SUCH SONG FOUND IN DISCOGRAPHY."
        return client_answer

"""
input: data.
output: client_answer
returns the album of the requested song.
"""
def ALS(data):
    client_answer = ""
    with open(FILE_PATH) as data_file:
        raw_data = data_file.read()
        raw_data = raw_data.split(ALBUM_IDENTIFIER)
        for album in range(AMOUNT_OF_ALBUMS):
            if data in raw_data[album]:
                info_line = raw_data[album].split(INFO_IDENTIFIER)
                album_name = info_line[0]
                client_answer = "ALBUM NAME: " + album_name + "."
        if client_answer == "":
            client_answer = "THERE'S NO SUCH SONG IN ANY ALBUM AT THE DISCOGRAPHY."
        return client_answer

"""
input: data.
output: client_answer
returns all the songs NAMES with that word in them.
"""
def SEA(data):
    client_answer = "SONGS NAMES: "
    with open(FILE_PATH) as data_file:
        for line in data_file:
            if SONG_IDENTIFIER in line:
                song_title = ((line[:line.index(INFO_IDENTIFIER)]).lower())[1:]
                if song_title.find(data) is not NOT_FOUND:
                    client_answer =  client_answer + song_title.title() + ", "
        if client_answer == "SONGS NAMES: ":
            client_answer = "NO SONGS WITH THAT WORD FOUND IN DISCOGRAPHY."
        client_answer = client_answer[:len(client_answer) - 2] + "."
        return client_answer

"""
input: data.
output: client_answer
returns all the songs with that word in them.
"""
def SE1(data):
    client_answer = "SONGS THE CONTAIN THE WORD, " + data + " IN THEM: "
    song_found = False
    with open(FILE_PATH) as data_file:
        raw_data = data_file.readlines()
        for lines in reversed(raw_data):
            if data in lines.lower() and ALBUM_IDENTIFIER not in lines and SONG_IDENTIFIER not in lines:
                song_found = True
                continue
            if song_found and SONG_IDENTIFIER in lines:
                client_answer = client_answer + lines[1:lines.index(INFO_IDENTIFIER)] + ", "
        if client_answer == "SONGS THE CONTAIN THE WORD, " + data + " IN THEM: ":
            client_answer = "NO SONGS WITH THAT WORD IN THEM."
        client_answer = client_answer[:len(client_answer) - 2] + ". "   
        return client_answer

"""
input: none.
output: client_answer
BONUS:prints popular words in discography.
""" 
def PWL():
    client_answer = ""
    common_list = []
    with open(FILE_PATH) as data_file:
        words = re.findall(r'\w+', data_file.read().lower())
        most_common = collections.Counter(words).most_common(AMOUNT_OF_WORDS)
        
        for item in most_common:
            common_list.append(item[0])
            
        client_answer = "MOST POPULAR WORDS: " + ', '.join(common_list) + "."
    
        return client_answer       
        
"""
input: none.
output: server_msg
BONUS:prints all albums in discography from shortest to longest.
""" 
def LAB():
    client_answer = ""
    album_lengths = []
    sorted_album_list = []
    with open(FILE_PATH) as data_file:
        raw_data = data_file.read()
        album_lengths = raw_data.split(ALBUM_IDENTIFIER)
        sorted_album_lengths = (sorted(album_lengths, key= len))[1:]
        for album in sorted_album_lengths:
            temp = album.split((INFO_IDENTIFIER)[0])[0]
            sorted_album_list.append(temp)
        client_answer = "SHORTEST TO LONGEST ALBUM: " + ', '.join(sorted_album_list) + "."
        return client_answer
       
        