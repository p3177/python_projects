# -*- coding: utf-8 -*-
"""
Created on Wed Apr 28 22:24:39 2021

@author: magshimim
"""
#version 1.0: Basic communication requrements.
#version 2.0: minor changes acording to  server code.
#version 3.0(pressented version): added exceptions, "basic client connection" body. documentation added.

#dict according to client answers (possible answers only) + port and ip.
SERVER_PORT = 9090
SERVER_IP = "127.0.0.1"
COMMANDS = {
1: ("Get Albums", "ALB"),
2: ("Get Album Songs","SNG"),
3: ("Get Song Length","LEN"),
4: ("Get Song Lyrics","LYR"),
5: ("Get Song Album","ALS"),
6: ("Search Song By Name","SEA"),
7: ("Search Song By Lyrics","SE1"),
8: ("Quit","QUI")
}

import socket

def main():
    server_msg = ""
    flag = True
    while flag == True and server_msg != "Quit#":
#object as "sock" for function usage.
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
#excpect Exception when trying to connect to server.
            try:
                sock.connect((SERVER_IP, SERVER_PORT))
                print(server_info_decode(sock)) 
            except Exception:    
                    print("Server couldn't be reached...Please try again later.")
                    break
            greeting_and_choice()

#if client hasn't chosen "Quit" function
            while server_msg != "Quit#":
                try:
                    server_msg = get_action()
                    if server_msg == "Quit#":
                      break
#excpect Exception when trying to send information to server. 
                    try: 
                        sock.sendall(server_msg.encode())
                        print(server_info_decode(sock))
                    except Exception:    
                        print("Server disconnected...Please try again later.")
                        flag = False
                        break
                except Exception:
                    print("Something went wrong with your input...")
    if server_msg == "Quit#":
        print("Thanks for using the Pink Floyd Information server.(:")

"""
input: sock
output: server_info
#decodes answer from server.   
""" 
def server_info_decode(sock):
    server_info =  sock.recv(1024)
    server_info = server_info.decode()
    return server_info

def greeting_and_choice():
    print("PINK FLOYD INFORMATION SERVER\n~~~~~~~~~~~~~~\ncommands:")
    print("1) Get Albums# - prints you all the albums of Pink Floyd.")
    print("2) Get Album Songs#DATA - prints you all the songs of the requested album.")
    print("3) Get Song Length#DATA - prints you all the length of the requested song.")    
    print("4) Get Song Lyrics#DATA - prints you all the Lyrics of the requested song.")    
    print("5) Get Song Album#DATA - prints you the album of the requested song.") 
    print("6) Search Song By Name#DATA - prints you all the songs NAMES with that word in them.")
    print("7) Search Song By Lyrics#DATA - prints you all the songs with that word in them.") 
    print("8) Quit.")
    
def get_action():
    choice = int(input("What would you like to choose?: " ))
    if choice == 8:
        choice = "Quit#"
        return choice
    elif choice == 1:    
        data = ""
    else:
        data = input("please enter more information as needed: ")
    
    server_msg = str(((COMMANDS[choice])[0]) + "#" + data.title())
    return server_msg
       
        
if __name__ == "__main__":
    main()
"""
    data_request = data.split(" ")
    data = ""
    for word in data_request:
        data = data + " " + word.capitalize()
    """