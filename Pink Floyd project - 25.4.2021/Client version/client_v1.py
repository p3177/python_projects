# -*- coding: utf-8 -*-
"""
Created on Wed Apr 28 22:24:39 2021

@author: magshimim
"""
#version 1.0: Basic communication requrements.

SERVER_PORT = 9090
SERVER_IP = "127.0.0.1"
COMMANDS = {
1: ("Get Albums", "ALB"),
2: ("Get Album Songs","SNG"),
3: ("Get Song Length","LEN"),
4: ("Get Song Lyrics","LYR"),
5: ("Get Song Album","ALS"),
6: ("Search Song By Name","SEA"),
7: ("Search Song By Lyrics","SE1"),
8: ("Quit","QUI")
}

import socket

def main():
    choice = ""
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
        sock.connect((SERVER_IP, SERVER_PORT))
        greeting_and_choice()
        print(server_info_decode(sock))
        while choice != "Quit":
            choice = COMMANDS[int(input("What would you like to choose?: " ))][0]
            sock.sendall(choice.encode())
            print(server_info_decode(sock))
    
    
def server_info_decode(sock) :
    server_info =  sock.recv(1024)
    server_info = server_info.decode()
    return(server_info)  

def greeting_and_choice():
    print("PINK FLOYD INFORMATION SERVER\n~~~~~~~~~~~~~~\ncommands:")
    print("1) Get Albums# - prints you all the albums of Pink Floyd.")
    print("2) Get Album Songs#DATA - prints you all the songs of the requested album.")
    print("3) Get Song Length#DATA - prints you all the length of the requested song.")    
    print("4) Get Song Lyrics#DATA - prints you all the Lyrics of the requested song.")    
    print("5) Get Song Album#DATA - prints you all the Lyrics of the requested song.") 
    print("6) Search Song By Name#DATA - prints you all the songs NAMES with that word in them.")
    print("7) Search Song By Lyrics#DATA - prints you all the songs with that word in them.") 
    print("8) Quit.")

        
        
if __name__ == "__main__":
    main()
