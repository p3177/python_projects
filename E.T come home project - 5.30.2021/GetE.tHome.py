import hashlib
from scapy.layers.inet import IP, UDP
from scapy.layers.l2 import Ether
from scapy.packet import Raw
from scapy.sendrecv import sniff, srp1

HASH_FLAG = "location data"
SPORT = 0

def decode_message(text):
    allowed_letters = (' ', ',', '!', '.', ':', '*', ';', '/', '=', "b'", "'", "_")
    result = ''
    key = int(text[3:6])
    for index, letter in enumerate(text[6:len(text)]):
        if index % 2 == 1 or letter in allowed_letters or letter.isnumeric():
            result += letter
        else:
            result += chr((ord(letter) - 97 - key) % 26 + 97)
    return result

def incode_message(text, key):
    allowed_letters = (' ', ',', '!', '.', ':', '*', ';', '/', '=', "b'", "'", "_")
    result = ''
    for index, letter in enumerate(text[:len(text)]):
        if index % 2 == 1 or letter in allowed_letters or letter.isnumeric():
            result += letter
        else:
            result += chr((ord(letter) - 97 + key) % 26 + 97)
    return result

def Et_filter(packet):
    global SPORT
    if IP in packet and packet[IP].src == "54.71.128.194" and SPORT == 0:
        SPORT = packet[UDP].dport
    return UDP in packet and IP in packet and (packet[IP].src == "54.71.128.194" or packet[IP].dst == "54.71.128.194")

def stop_filter(packet):
    decrypted_data = decode_message(str((packet[Raw].load))[2:])
    return "location data 10/10:" in decrypted_data

def print_Et(packet):
    decrypted_data = decode_message(str((packet[Raw].load))[2:])
    print(decrypted_data)

def main():
    to_hash = ""
    packets = sniff(lfilter=Et_filter, prn=print_Et, stop_filter=stop_filter)
    for packet in packets:
        decrypted_data = decode_message(str((packet[Raw].load))[2:])
        if HASH_FLAG in decrypted_data:
            splited_data = decrypted_data.split(": ")
            to_hash += (splited_data[1])[:len(splited_data[1]) - 1]
    flight_req = "FLY008" + incode_message("location_md5=" + str(hashlib.md5(to_hash.encode()).hexdigest()) + ",airport=nevada25.84,time=15:52,lane=earth.jup,vehicle=2554,fly", 8)
    print(flight_req)
    full_msg = Ether() / IP(dst="54.71.128.194") / UDP(dport=99, sport=SPORT) / Raw(load= flight_req)
    ans = srp1(full_msg, verbose=0)

if __name__ == "__main__":
    main()
