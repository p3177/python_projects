#version 1.0: successful game fixing, basic game function setting.
#version 2.0: unsuccessful game "LOSE" print fixing + removing debug features.
#version 3.0(FINAL VERSION): proper spacing between code structure + function Documentation.

# -*- coding: utf-8 -*-
"""
Created on Fri Mar 26 14:06:57 2021

@author: magshimim
"""

def main():
    num_of_tries = 0
    MAX_TRIES = 6
    old_letters_guessed = []
    dont_show_hangman = 0
    dont_show_hidden_word = 0

#gretting + user input for secret word
    gretting()
    file_path = input("Enter file path: ")
    index = int(input("Enter index: "))
    secret_word = choose_word(file_path, index)

    print("\nLet's start!\n")

#while loop for entire game
    while  (check_win(secret_word, old_letters_guessed) == False):

        if (num_of_tries == MAX_TRIES):
            print_hangman(num_of_tries)
            print(show_hidden_word(secret_word, old_letters_guessed))
            print("LOSE")
            break

#prints game visuals according to game state
        if (dont_show_hangman == 0):
            print_hangman(num_of_tries)
        if (dont_show_hidden_word == 0):
            print(show_hidden_word(secret_word, old_letters_guessed))

#asks user to enter a guess + sends it to function
        letter_guessed = input("Guess a letter: ")
        num_of_tries, dont_show_hangman, dont_show_hidden_word = try_update_letter_guessed(letter_guessed, old_letters_guessed, secret_word, num_of_tries, dont_show_hangman, dont_show_hidden_word)

#announces win to player
    if (check_win(secret_word, old_letters_guessed) == True):
        spaceitout(secret_word)
        print("WIN")

             
#spaces out secret word
def spaceitout(secret_word):
    pile = ""
    for letter in secret_word:
        pile = pile + letter + " "
    pile = pile[:-1]
    print(pile)

#declares if the player wins
def check_win(secret_word, old_letters_guessed):
    for item in secret_word:
        if item not in old_letters_guessed:
            return False
    return True

def try_update_letter_guessed(letter_guessed, old_letters_guessed, secret_word, num_of_tries, dont_show_hangman, dont_show_hidden_word):
#method. setting error flags as false.
    letter_guessed = letter_guessed.lower()
    Error1 = False
    Error2 = False
    Error3 = False

#checks for errors 
    if(len(letter_guessed) > 1):
        Error1 = True

#using .isalpha() method to check if all are letters
    if(letter_guessed.isalpha() == False):
        Error2 = True
    
    if(letter_guessed in old_letters_guessed):
        Error3 = True

#prints type of error to user
    if((Error1 == True) and (Error2 == False) and (Error3 == False)):
        dont_show_hangman = 0
        dont_show_hidden_word = 0
        return num_of_tries, dont_show_hangman, dont_show_hidden_word

    elif((Error2 == True) and (Error1 == False) and (Error3 == False)):
        print("X")
        dont_show_hangman = 1
        dont_show_hidden_word = 1
        return num_of_tries, dont_show_hangman, dont_show_hidden_word

    elif((Error1 == True) and (Error2 == True) and (Error3 == False)):
        dont_show_hangman = 1
        dont_show_hidden_word = 1
        print("X")
        print(' -> '.join(sorted(old_letters_guessed)).lower())
        return num_of_tries, dont_show_hangman, dont_show_hidden_word

    elif((Error1 == False) and (Error2 == False) and (Error3 == True)):
        dont_show_hangman = 1
        dont_show_hidden_word = 1
        print("X")
        print(' -> '.join(sorted(old_letters_guessed)).lower())
        return num_of_tries, dont_show_hangman, dont_show_hidden_word

#if no error flags were turned on:
    else:
        old_letters_guessed.append(letter_guessed)
        if letter_guessed in secret_word:
            dont_show_hangman = 1
            dont_show_hidden_word = 0
            return num_of_tries, dont_show_hangman, dont_show_hidden_word
        else:
            print(":(")
            dont_show_hangman = 0
            dont_show_hidden_word = 0
            num_of_tries =  num_of_tries + 1
            return num_of_tries, dont_show_hangman, dont_show_hidden_word
            

#converts chosen word according to the state of the game
def show_hidden_word(secret_word, old_letter_guessed):
    new_str = ""
    for item in secret_word:
        if item in old_letter_guessed:
            new_str += item
        if item not in old_letter_guessed:
            new_str += "_"
    return " ".join(new_str)



#function chooses word from path provided by user
def choose_word(file_path, index):
	with open(file_path, 'r') as words_file:
		words=words_file.read()
	words=words.split()
	length_of_words = len(words)
	while (index > length_of_words):
		index -= length_of_words
	word = words[index-1]
	words_file = sorted(list(set(words)), key=words.index)
	return word.lower()



#function prints hangman states according to num_of_tries
def print_hangman(num_of_tries):
    
    HANGMAN_PHOTOS = {"picture 1": r"""x-------x""", "picture 2": """
        x-------x
        |
        |
        |
        |
        |
        """, "picture 3": """
        x-------x
        |       |
        |       0
        |
        |
        |
        """, "picture 4": """ 
        x-------x
        |       |
        |       0
        |       |
        |
        |
    """, "picture 5":r"""
        x-------x
        |       |
        |       0
        |      /|\
        |
        |
        """, "picture 6":r"""
        x-------x
        |       |
        |       0
        |      /|\
        |      /
        |
        """, "picture 7":r""" 
        x-------x
        |       |
        |       0
        |      /|\
        |      / \
        |
        """}
        
    if num_of_tries == 0:
        print(HANGMAN_PHOTOS["picture 1"])
    
    elif num_of_tries == 1:
        print(HANGMAN_PHOTOS["picture 2"])
    
    elif num_of_tries == 2:
        print(HANGMAN_PHOTOS["picture 3"])
    
    elif num_of_tries == 3:
        print(HANGMAN_PHOTOS["picture 4"])
    
    elif num_of_tries == 4:
        print(HANGMAN_PHOTOS["picture 5"])
    
    elif num_of_tries == 5:
        print(HANGMAN_PHOTOS["picture 6"])
    
    elif num_of_tries == 6:
        print(HANGMAN_PHOTOS["picture 7"])



#function prints greeting to player
def gretting():
    
    print(r"""           _    _                                         
          | |  | |                                        
          | |__| | __ _ _ __   __ _ _ __ ___   __ _ _ __  
          |  __  |/ _` | '_ \ / _` | '_ ` _ \ / _` | '_ \ 
          | |  | | (_| | | | | (_| | | | | | | (_| | | | |
          |_|  |_|\__,_|_| |_|\__, |_| |_| |_|\__,_|_| |_|
                               __/ |                      
                              |___/      """)
                              
if __name__ == "__main__":
    main()
